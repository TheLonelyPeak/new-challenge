import { v4 as uuidv4 } from 'uuid'
import { createContext, useContext, useEffect, useState } from 'react'
//From firebase.js:
import { auth, db } from '../firebase'
//From Firebase Authentication dependency:
import { createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut, onAuthStateChanged } from 'firebase/auth'
//From Firebase Firestore dependency:
import { setDoc, doc } from 'firebase/firestore'

const AuthContext = createContext()

export function AuthContextProvider({ children }) {
	const [user, setUser] = useState(null)

	let id = uuidv4()

	function signUp(email, password, name) {
		createUserWithEmailAndPassword(auth, email, password)
		const userRef = doc(db, 'users', id)
		setDoc(userRef, { email, name, id, hasPayed: false, comments: '' })
	}

	function logIn(email, password) {
		return signInWithEmailAndPassword(auth, email, password)
	}

	function logOut() {
		return signOut(auth)
	}

	//on-off state change
	useEffect(() => {
		const unsubscribe = onAuthStateChanged(auth, (currentUser) => {
			setUser(currentUser)
		})
		return () => {
			unsubscribe()
		}
	})

	//value -> sånt som ska vara tillgängligt för andra komponenter
	return <AuthContext.Provider value={{ signUp, logIn, logOut, user }}>{children}</AuthContext.Provider>
}

export function UserAuth() {
	return useContext(AuthContext)
}
