/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    fontFamily: {
      mulish: ["Mulish"],
      circular500: ["Circular Std", "sans-serif"],
    },
    extend: {
      colors: {
        projectBlue: "#90BEDE",
        headerBlue: "#3B3FA4",
      },
      fontFamily: {
        poppins: ["font-family: 'Poppins', sans-serif;"],
      },
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
    },
  },
  plugins: [],
};
