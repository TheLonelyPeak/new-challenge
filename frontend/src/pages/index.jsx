import Layout from '../components/layout'
// import AdminLogin from "./adminLogin";
import Link from 'next/link'

import splash from '../assets/splash.png'
import hero1 from '../assets/hero1.png'
import hero2 from '../assets/hero2.png'
import hero3 from '../assets/hero3.png'
import chaslogo from '../assets/Logo.png'
import Buttons from '@/components/buttons'
import Image from 'next/image'
import Logout from './logout'

export default function Home() {
	return (
		<Layout>
			<div className='ml-[104px] mb-10 '>
				<Logout />
			</div>

			<div className=''>
				<div className='flex flex-col justify-center items-center gap-11'>
					<Image className=' object-cover w-[393px] h-[556px] md:w-[1414px]' src={splash} />
					<div className='flex flex-col items-center '>
						<div className='flex flex-col font-circular gap-8 md:hidden '>
							<Buttons defaultButton={'primary'} fixed={'lg'} title='Bli medlem' />
							<Buttons defaultButton={'orange'} fixed={'lg'} title='Kontakta oss' />
						</div>
					</div>
					<div className=' md:flex md:h-[669px] md:w-[1414px]'>
						<div className=' flex flex-col mt-[63px] md:mt-0 gap-[33px] h-[890px] md:h-[381px] md:items-center '>
							<h3 className='text-[#3B3FA4] w-[307px] h-[86px] font-semibold  text-[32px]/[40px] md:text-[40px]/[51px] md:w-[425px] md:h-[59px] md:ml-[176px] md:font-[900]'>
								Primus Inter Pares – Främst bland likar
							</h3>
							<p className=' w-[296px] h-[771px] font-medium text-2xl/[30.36px] text-[#222222] md:w-[574px] md:h-[246px] md:text-[20px]/[25px] md:mt-[76px] md:ml-[119px] md:font-[450]'>
								Studentationerna är ideella organisationer som finns till för studenterna och deras välbefinnande. Vi på Chas SK har många olika
								verksamheter, föreningar och aktiviteter för att du ska hitta något som passar just dig. Vi anordnar baler, gasquer, fester och många
								andra evenemang. Hos oss kan du också äta frukost och lunch, ta en öl på puben med dina vänner, fika med en kopp kaffe eller sitta och
								plugga. Om du vill lära känna nya människor, utvecklas som person och kanske tjäna en slant ska du engagera dig här på Kåren! Det är
								otroligt givande, ger dig många skratt och du får vänner för livet.
							</p>
							<div className='md:flex md:font-circular md:gap-[30px] hidden md:mt-[40px] md:ml-[100px]'>
								<Buttons defaultButton={'orange'} fixed={'lg'} title='Kontakta oss' />
								<Buttons defaultButton={'primary'} fixed={'lg'} title='Bli medlem' />
							</div>
						</div>
						<div className='hidden md:flex '>
							<Image className='md:w-[279.99px] md:h-[303.89px] md:absolute md:top-[992px] md:left-[915.06px] md:ml-[105.94px]' src={hero1} />
							<Image className='md:w-[339.39px] md:h-[225.97px] md:absolute md:top-[1294px] md:left-[969.61px] md:ml-[113px] ' src={hero3} />
							<Image className='md:w-[245.02px] md:h-[245.02px] md:absolute md:top-[1232.69px] md:left-[770px] md:ml-[116px]' src={hero2} />
						</div>
					</div>
					<div className='w-[265px] h-[40px] text-center mb-[74px] md:hidden '>
						<p className='text-[#3B3FA4] text-[32px]/[40.48px] font-medium'>Primus Inter Pares</p>
					</div>
				</div>
			</div>

			<div className='w-[393px] h-[1178px] md:w-[1414px] md:h-[673px] bg-[#E5E1EE] flex md:flex-col justify-evenly'>
				<h1 className='hidden md:text-center text-[#3B3FA4] md:h-[59px] md:font-black md:text-[40px]/[51px] md:not-italic'>loreum ipsum totalias</h1>
				<div className='flex flex-col justify-evenly items-center md:flex-row'>
					<div className=' w-[346.64px] h-[327px] md:w-[459px] md:h-[433px] bg-white rounded-[30px] flex flex-col justify-evenly '>
						<p className='text-[#3B3FA4] text-[18.1247px]/[23px] text-center font-medium md:text-[24px]/[30.36px] '>Nyheter</p>
						<div className='flex justify-evenly items-center '>
							<p className='w-[32.47px] h-[21.15px] md:w-[43px] text-center md:h-[28px] text-[15.1039px]/[21px] font-light text-[#3B3FA4] md:text-[20px]/[28.22px] '>
								7/2
							</p>
							<p className='border-r-[#E7A422] border-r-[2.27px] h-[51.35px] md:border-r-[3px] md:h-[68px]'></p>
							<p className='text-[#3B3FA4] w-[225.05px] h-[42.29px] text-[16.6143px]/[21px] font-[450] md:text-[22px]/[27.83px] md:w-[298px] md:h-[56px]'>
								Borem ipsum dolor sit amet, consectetur adipiscing elit
							</p>
						</div>
						<div className='flex justify-evenly items-center'>
							<p className='w-[32.47px] h-[21.15px] md:w-[43px] text-center md:h-[28px] text-[15.1039px]/[21px] font-light text-[#3B3FA4] md:text-[20px]/[28.22px]'>
								11/2
							</p>
							<p className='border-r-[#E7A422] border-r-[2.27px] h-[51.35px] md:border-r-[3px] md:h-[68px]'></p>
							<p className='text-[#3B3FA4] w-[225.05px] h-[42.29px] text-[16.6143px]/[21px] font-[450] md:text-[22px]/[27.83px] md:w-[298px] md:h-[56px]'>
								Borem ipsum dolor sit amet, consectetur adipiscing elit
							</p>
						</div>
						<div className='flex justify-evenly items-center'>
							<p className='w-[32.47px] h-[21.15px] md:w-[43px] text-center md:h-[28px] text-[15.1039px]/[21px] font-light text-[#3B3FA4] md:text-[20px]/[28.22px]'>
								15/2
							</p>
							<p className='border-r-[#E7A422] border-r-[2.27px] h-[51.35px] md:border-r-[3px] md:h-[68px]'></p>
							<p className='text-[#3B3FA4] w-[225.05px] h-[42.29px] text-[16.6143px]/[21px] font-[450] md:text-[22px]/[27.83px] md:w-[298px] md:h-[56px]'>
								Borem ipsum dolor sit amet, consectetur adipiscing elit
							</p>
						</div>
						<div className=' flex justify-center'>
							<button className='text-[#E7A422] text-[15.1px]/[21.31px] font-light md:text-[20px]/[28.22px]'>Visa alla nyheter...</button>
						</div>
					</div>
					<div className='w-[254px] h-[235.61px] md:w-[296.46px] md:h-[275px]'>
						<Image src={chaslogo} />
					</div>
					<div className='w-[346.64px] h-[327px] md:w-[459px] md:h-[433px] bg-white rounded-[30px] flex flex-col justify-evenly '>
						<p className='text-[#3B3FA4] text-[18.1247px]/[23px] text-center font-medium md:text-[24px]/[30.36px]'>Evenemang</p>
						<div className='flex justify-evenly items-center'>
							<p className='w-[32.47px] h-[21.15px] md:w-[43px] text-center md:h-[28px] text-[15.1039px]/[21px] font-light text-[#3B3FA4] md:text-[20px]/[28.22px] '>
								7/2
							</p>
							<p className='border-r-[#E7A422] border-r-[2.27px] h-[51.35px] md:border-r-[3px] md:h-[68px]'></p>
							<p className='text-[#3B3FA4] w-[225.05px] h-[42.29px] text-[16.6143px]/[21px] font-[450] md:text-[22px]/[27.83px] md:w-[298px] md:h-[56px]'>
								Borem ipsum dolor sit amet, consectetur adipiscing elit
							</p>
						</div>
						<div className='flex justify-evenly items-center'>
							<p className='w-[32.47px] h-[21.15px] md:w-[43px] text-center md:h-[28px] text-[15.1039px]/[21px] font-light text-[#3B3FA4] md:text-[20px]/[28.22px] '>
								11/2
							</p>
							<p className='border-r-[#E7A422] border-r-[2.27px] h-[51.35px] md:border-r-[3px] md:h-[68px]'></p>
							<p className='text-[#3B3FA4] w-[225.05px] h-[42.29px] text-[16.6143px]/[21px] font-[450] md:text-[22px]/[27.83px] md:w-[298px] md:h-[56px]'>
								Borem ipsum dolor sit amet, consectetur adipiscing elit
							</p>
						</div>
						<div className='flex justify-evenly items-center'>
							<p className='w-[32.47px] h-[21.15px] md:w-[43px] text-center md:h-[28px] text-[15.1039px]/[21px] font-light text-[#3B3FA4] md:text-[20px]/[28.22px]'>
								15/2
							</p>
							<p className='border-r-[#E7A422] border-r-[2.27px] h-[51.35px] md:border-r-[3px] md:h-[68px]'></p>
							<p className='text-[#3B3FA4] w-[225.05px] h-[42.29px] text-[16.6143px]/[21px] font-[450] md:text-[22px]/[27.83px] md:w-[298px] md:h-[56px]'>
								Borem ipsum dolor sit amet, consectetur adipiscing elit
							</p>
						</div>
						<div className=' flex justify-center'>
							<button className='text-[#E7A422] text-[15.1px]/[21.31px] font-light md:text-[20px]/[28.22px]'>Visa alla evenemang...</button>
						</div>
					</div>
				</div>
			</div>
		</Layout>
	)
}
