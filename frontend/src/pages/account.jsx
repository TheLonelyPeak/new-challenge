import React from 'react'
import { useRouter } from 'next/router'
import { withProtected } from '../protectedRoute'
import Link from 'next/link'
import { useState } from 'react'
import { db } from '../../firebase'
import { collection, query, getDocs, deleteDoc, doc, updateDoc } from 'firebase/firestore'
import Modal from '../components/modal'
import { UserAuth } from '../../context/AuthContext'

function Account() {
	const { user, logOut } = UserAuth()
	const [users, setUsers] = useState([])
	const [isGetUsersOpen, setIsGetUsersOpen] = useState(false)
	const [isUpdateUserOpen, setIsUpdateUserOpen] = useState(false)
	const [error, setError] = useState('')
	const [isLogoutOpen, setIsLogoutOpen] = useState(false)
	const router = useRouter()

	const [newComment, setNewComment] = useState('')
	const [isInstructionsOpen, setIsInstructionsOpen] = useState(false)
	const [hasPayed, setHasPayed] = useState(false)

	const [checkedUsers, setCheckedUsers] = useState({})

	const usersRef = collection(db, 'users')
	const usersQuery = query(usersRef)

	const handleLogout = async () => {
		try {
			setIsLogoutOpen(!isLogoutOpen)
		} catch (error) {
			console.log(error)
			setError(error)
		}
	}

	const handleLogoutClose = () => {
		setIsLogoutOpen(!isLogoutOpen)
		logOut()
		console.log('logged out', user.name)
		router.push('/')
	}

	function handleGetUsers() {
		getUsers().then((users) => {
			setUsers(users)
			setIsGetUsersOpen(!isGetUsersOpen)
		})
	}

	function getUsers() {
		return getDocs(usersQuery)
			.then((querySnapshot) => {
				const users = []
				querySnapshot.forEach((doc) => {
					users.push(doc.data())
				})
				//Filter out deleted user
				setUsers((prevUsers) =>
					prevUsers.filter((user) => {
						return users.some((updatedList) => updatedList.id === user.id)
					})
				)
				return users
			})
			.catch((error) => {
				console.log('Error getting users: ', error)
			})
	}

	function togglePopupForm() {
		setIsUpdateUserOpen(!isUpdateUserOpen)
	}

	const deleteUser = async (id) => {
		try {
			const docRef = doc(db, 'users', id)
			await deleteDoc(docRef)

			//Hämta uppdaterad lista med användare
			getUsers()
		} catch (err) {
			console.error(err)
		}
	}

	const updateComments = async (id) => {
		const docRef = doc(db, 'users', id)
		await updateDoc(docRef, { comments: newComment })
		getUsers().then((users) => {
			setUsers(users)
		})
	}

	const handleModalClose = async () => {
		setIsInstructionsOpen(!isInstructionsOpen)
	}

	const handleHasPayed = async (e, id) => {
		const isChecked = e.target.checked
		setCheckedUsers({ ...checkedUsers, [id]: isChecked })
		const docRef = doc(db, 'users', id)
		await updateDoc(docRef, { hasPayed: isChecked })
	}

	return (
		<>
			<div className='mr-52 ml-52 w-[80vw]'>
				<div className='mt-10 gap-10 flex justify-center '>
					<div>
						<button onClick={handleGetUsers} className='bg-[#363B59] text-white py-2 px-3 mb-2 text-sm rounded font-medium'>
							Hämta användare
						</button>
					</div>
					<div>
						<button onClick={handleLogout} className='bg-[#363B59] text-white py-2 px-3 mb-2 text-sm rounded font-medium'>
							Logga ut
						</button>
					</div>
				</div>

				{isLogoutOpen ? (
					<Modal>
						<div className='flex flex-col gap-4 bg-white px-8 py-8 rounded-lg'>
							<h2 className='text-md mt-2 font-semibold pr-48'>Du har blivit utloggad!</h2>

							<div className='flex flex-row'>
								<button onClick={handleLogoutClose} className='py-2 px-4 bg-[#ED5E68] text-white font-bold text-md rounded'>
									Stäng
								</button>
							</div>
						</div>
					</Modal>
				) : null}

				{isGetUsersOpen ? (
					<div className='mt-10 gap-10 grid grid-cols-3 justify-center items-center'>
						{users
							.sort((a, b) => (a.name > b.name ? 1 : -1))
							.map((user, i) => {
								return (
									<div className='border-2 border-gray-300 w-xs p-6 rounded-md flex flex-col gap-4 text-sm font-medium'>
										<div key={i} style={{ color: !user.hasPayed && '#ED5E68' }}>
											<div className='mb-4'>
												<span>Namn:</span> {user.name}
											</div>
											<div className='mb-4'>
												<span>E-post:</span> {user.email}
											</div>
											<div className='mb-4'>
												<span>Kommentar:</span> <div className='font-normal text-xs'>{user.comments}</div>
											</div>
										</div>
										<div>
											<button onClick={() => deleteUser(user.id)} className='bg-[#ED5E68] text-white py-2 px-3 mb-2 mt-4 text-sm rounded'>
												Ta bort
											</button>
											<div className='text-xs font-normal'>
												Obs! Glöm inte att ta bort användaren i Firebase Authentication! Anvisningar finns{' '}
												<span onClick={() => setIsInstructionsOpen(!isInstructionsOpen)} className='hover:underline font-bold cursor-pointer'>
													här
												</span>
												.
											</div>
											{isInstructionsOpen ? (
												<Modal>
													<div className='flex flex-col gap-4 bg-white px-8 py-8 rounded-lg w-[40rem]'>
														<h2 className='text-sm mt-2 font-semibold pr-48'>Så här tar du bort en användare i Firebase Authentication</h2>
														<p className='font-normal text-xs'>
															Logga in till Firebase, gå till fliken <span className='font-semibold'>Authentication</span>, hovra över den användare
															du vill ta bort, klicka på de tre punkterna som visas längst till höger och välj{' '}
															<span className='font-semibold'>Delete</span>. Kontakta admin om du behöver inloggningsuppgifter till Firebase.
														</p>

														<div className='flex flex-row'>
															<button onClick={handleModalClose} className='py-2 px-4 bg-[#ED5E68] text-white font-bold text-md rounded'>
																Stäng
															</button>
														</div>
													</div>
												</Modal>
											) : null}
										</div>

										<div className='mt-8'>
											<button onClick={togglePopupForm} className='bg-[#363B59] text-white py-2 px-3 mb-2 text-sm rounded'>
												Uppdatera
											</button>

											{isUpdateUserOpen ? (
												<div className='mt-8 p-6 flex flex-col item-center justify-center gap-2 placeholder-gray-300 placeholder-opacity-50 rounded-md border-2 border-gray-300'>
													<textarea
														type='textarea'
														onChange={(e) => setNewComment(e.target.value)}
														placeholder='Ny kommentar'
														className='bg-gray-100 p-1 rounded'
													/>

													<div className='flex gap-2'>
														<input
															type='checkbox'
															checked={checkedUsers[user.id]}
															onChange={(e) => handleHasPayed(e, user.id)}
															className='accent-black'
														/>
														<label>Har betalat?</label>
													</div>
													<button onClick={() => updateComments(user.id)} className='mt-6 bg-[#363B59] text-white py-2 px-3 mb-2 text-sm rounded'>
														Bekräfta
													</button>
												</div>
											) : null}
										</div>
									</div>
								)
							})}
					</div>
				) : null}

				<p className='font-semibold text-center mt-20'>
					<Link href='/' className='underline underline-offset-4'>
						Tillbaka till startsidan
					</Link>
				</p>
			</div>
		</>
	)
}

export default withProtected(Account)
