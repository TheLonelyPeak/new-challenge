import Link from 'next/link'
import React from 'react'

function Unauthorized() {
	return (
		<div className='w-96 h-auto bg-slate-300 mx-auto p-4 rounded-md mt-20'>
			<div>Du är inte behörig! Gå till startsidan och logga in om du redan har ett konto eller registrera dig!</div>
			<Link href='/' className='underline font-semibold'>
				Till startsidan
			</Link>
		</div>
	)
}

export default Unauthorized
