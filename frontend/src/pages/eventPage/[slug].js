import React from 'react'

import fs from 'fs'
import path from 'path'
import matter from 'gray-matter'
import { marked } from 'marked'
import Link from 'next/link'
import Layout from '@/components/layout'

function EventPage({ frontmatter: { title, date, cover_image }, slug, content }) {
	return (
		<Layout>
			<div className='flex justify-center'>
				<div className='m-14 text-sm max-w-[60vw] rounded-md p-8 shadow-xl'>
					<div className='card card-page mt-8'>
						<h1 className='text-xl font-semibold mb-6'>{title}</h1>
						<div className='mb-2 text-xs'>Posted on {date}</div>
						<img src={cover_image} alt='' className='md-img' />
						<div className='mt-10'>
							<div dangerouslySetInnerHTML={{ __html: marked(content) }}></div>
						</div>
					</div>
				</div>
			</div>
		</Layout>
	)
}

export default EventPage

//To generate the static paths
export async function getStaticPaths() {
	const files = fs.readdirSync(path.join('./src/events'))

	const paths = files.map((filename) => ({
		params: {
			slug: filename.replace('.md', ''),
		},
	}))

	console.log(paths)

	return {
		paths,
		fallback: false,
	}
}

//To get the data
export async function getStaticProps({ params: { slug } }) {
	const markdownWithMeta = fs.readFileSync(path.join('./src/events', slug + '.md'), 'utf-8')

	const { data: frontmatter, content } = matter(markdownWithMeta)
	console.log(slug)
	return {
		props: {
			frontmatter,
			slug,
			content,
		},
	}
}
