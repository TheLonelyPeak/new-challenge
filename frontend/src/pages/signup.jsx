import React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { UserAuth } from '../../context/AuthContext'
import { useState } from 'react'
import Layout from '@/components/layout'
import Modal from '@/components/modal'
import grå from '../assets/grå.jpg'
import Image from 'next/image'
function Signup() {
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [name, setName] = useState('')
	const [isOpen, setIsOpen] = useState(false)
	const { user, signUp } = UserAuth()
	const router = useRouter()
	const handleSubmit = async (e) => {
		e.preventDefault()
		try {
			await signUp(email, password, name)
			setIsOpen(!isOpen)
		} catch (error) {
			console.log(error)
		}
	}
	const handleModalClose = () => {
		setIsOpen(!open)
		router.push('/')
	}
	return (
		<Layout>
			<div className=''>
				<div>
					<Image className='h-[200px] w-full' src={grå} />
				</div>
				<div className='w-full bg-[#E7A422] rounded'>
					<div className='mx-auto '>
						<h1 className='text-2xl text-[#3B4256] font-bold flex justify-center pt-10 pb-5'>Bli medlem här</h1>
						<form onSubmit={handleSubmit} className='w-full flex items-center flex-col pt-3 '>
							<input
								onChange={(e) => setName(e.target.value)}
								className='rounded p-2 my-2 w-1/3'
								type='text'
								placeholder='Fullständigt namn*'
								required
							/>
							<input
								onChange={(e) => setEmail(e.target.value)}
								className='p-2 my-2 rounded w-1/3'
								type='email'
								placeholder='E-postadress*'
								autoComplete='email'
								required
							/>
							<input
								onChange={(e) => setPassword(e.target.value)}
								className='p-2 my-2 rounded w-1/3'
								type='password'
								placeholder='Lösenord*'
								autoComplete='current-password'
								required
							/>
							<button
								type='submit'
								className='bg-[#026E78] w-[343px] h-[48px] py-[12px] px-[20px] text-lg flex-none order-none grow  not-italic leading-[100%] text-[16px] rounded-full mt-2 shadow-lg font-semibold shadow-[0_1px_2px_rgba(198, 228, 246, 0.05)] border border-[#026E78] text-white hover:bg-[#015364] hover:shadow-[0_1px_2px_rgba(198,228,246,0.05] hover:border hover:border-[#015364] focus:shadow-[0_1px_2px_rgba(16,24,40,0.05),0_0px_0px_4px_rgba(rgba(148, 243, 218, 1)] focus:ring-4 focus:ring-[#94f3da]'>
								Bli medlem
							</button>
							<p className='pt-8'>
								<span className=' text-[#3B4256] font-medium text-2xl'>Har du redan ett konto?</span>{' '}
								<Link href='/' className='underline text-[#3B4256] font-medium text-2xl hover:text-white'>
									Logga in här
								</Link>
							</p>
							<p className='pt-4 text-xs pb-10'>* = obligatoriskt fält</p>
						</form>
					</div>
				</div>
				{isOpen ? (
					<Modal>
						<div className='flex flex-col gap-4 bg-white px-8 py-8 rounded-lg'>
							<h2 className='text-md mt-2 font-semibold pr-48'>Registreringen är klar!</h2>
							<div className='flex flex-row'>
								<button
									onClick={handleModalClose}
									className='rounded-full bg-[#026E78] py-[10px] px-[16px] shadow-[0_1px_2px_rgba(198, 228, 246, 0.05)] border border-[#026E78] text-white hover:bg-[#015364] hover:shadow-[0_1px_2px_rgba(198,228,246,0.05] hover:border hover:border-[#015364] focus:shadow-[0_1px_2px_rgba(16,24,40,0.05),0_0px_0px_4px_rgba(rgba(148, 243, 218, 1)] focus:ring-4 focus:ring-[#94f3da] w-32 text-sm mt-2 font-normal not-italic leading-[100%]  text-[14px]'>
									Stäng
								</button>
							</div>
						</div>
					</Modal>
				) : null}
			</div>
		</Layout>
	)
}
export default Signup
