import React from 'react'
import { useRouter } from 'next/router'
import { UserAuth } from '../../context/AuthContext'
import { useState } from 'react'
import Link from 'next/link'
import Modal from '../components/modal'

function AdminLogin() {
	const [isOpen, setIsOpen] = useState(false)
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [error, setError] = useState('')
	const [isModalOpen, setIsModalOpen] = useState(false)
	const { user, logOut, logIn } = UserAuth()
	const router = useRouter()

	const handleLogout = async () => {
		try {
			await logOut()
			console.log('logged out', user.name)
			setIsModalOpen(!isModalOpen)
		} catch (error) {
			console.log(error)
			setError(error)
		}
	}

	const handleLogin = async (e) => {
		setError('')
		e.preventDefault()
		try {
			await logIn(email, password)
			router.push('/account')
		} catch (error) {
			console.log(error)
			setError('Du måste ha behörighet för att kunna logga in.')
			router.push('/')
		}
	}

	const handleModalClose = () => {
		setIsModalOpen(!isModalOpen)
	}

	const handleIsOpen = () => {
		setIsOpen(!isOpen)
		setError('')
	}

	return (
		<>
			<div className='flex items-center justify-between p-4 mt-2 z-[100] w-full'>
				{user ? (
					<button
						onClick={handleLogout}
						className="'bg-[#E7A422] w-auto h-[44px] py-[10px] px-[18px] flex-none order-none grow-0 font-normal not-italic leading-[100%]  text-[16px] rounded-full mt-2 border-[#E7A422] shadow-[0_1pxbg-[#E7A422] border_2px_rgba(198,228,246,0.05)] text-white hover:bg-gradient-to-br hover:from-[rgba(207,147,30)]  hover:to-[rgba(0,0,0,0.1)] hover:shadow-[0_1px_2px_rgba(198,228,246,0.05)] hover:border hover:border-[rgba(186,132,27)] focus:shadow-[0_1px_2px_rgba(16,24,40,0.05),0_0px_0px_4px_rgba(rgba(148, 243, 218, 1)] focus:ring-4 focus-[343px]  focus:ring-[#94f3da] cursor-pointer text-white m-2">
						Logga ut
					</button>
				) : (
					<div className='ml-6'>
						<button
							onClick={handleIsOpen}
							className='bg-[#E7A422] w-auto h-[44px] py-[10px] px-[18px] flex-none order-none grow-0 font-normal not-italic leading-[100%]  text-[16px] rounded-full mt-2 border-[#E7A422] shadow-[0_1pxbg-[#E7A422] border_2px_rgba(198,228,246,0.05)] text-white hover:bg-gradient-to-br hover:from-[rgba(207,147,30)]  hover:to-[rgba(0,0,0,0.1)] hover:shadow-[0_1px_2px_rgba(198,228,246,0.05)] hover:border hover:border-[rgba(186,132,27)] focus:shadow-[0_1px_2px_rgba(16,24,40,0.05),0_0px_0px_4px_rgba(rgba(148, 243, 218, 1)] focus:ring-4  focus:ring-[#94f3da]'>
							{isOpen ? 'Stäng' : 'Admin'}
						</button>
						{isOpen && (
							<div className='max-w-[400px] bg-[#E7A422] mt-5 pt-4 pb-2 pr-4 pl-4 mb-10 rounded'>
								<div className='mx-auto'>
									<h1 className='text-center  text-[#3B4256] font-medium text-lg'>Logga in (Admin)</h1>
									{error ? <p className='p-3  text-[#3B4256] font-medium text-sm my-2'>{error}</p> : null}
									<div className='w-full flex flex-col py-4'>
										<input
											onChange={(e) => setEmail(e.target.value)}
											className='rounded p-2 my-2 text-black'
											type='email'
											placeholder='E-postadress*'
											autoComplete='email'
											required
										/>
										<input
											onChange={(e) => setPassword(e.target.value)}
											className='rounded p-2 my-2 text-black'
											type='password'
											placeholder='Lösenord*'
											required
										/>
										<button
											onClick={handleLogin}
											className='rounded-full bg-[#026E78] py-[10px] px-[16px] shadow-[0_1px_2px_rgba(198, 228, 246, 0.05)] border border-[#026E78] text-white hover:bg-[#015364] hover:shadow-[0_1px_2px_rgba(198,228,246,0.05] hover:border hover:border-[#015364] focus:shadow-[0_1px_2px_rgba(16,24,40,0.05),0_0px_0px_4px_rgba(rgba(148, 243, 218, 1)] focus:ring-4 focus:ring-[#94f3da] w-32 text-sm mt-2 font-normal not-italic leading-[100%]  text-[14px]'>
											Logga in
										</button>
									</div>
									<p className='pt-4 text-xs  text-[#3B4256] font-medium'>* = obligatoriskt fält</p>
								</div>
							</div>
						)}
					</div>
				)}
				{isModalOpen ? (
					<Modal>
						<div className='flex flex-col gap-4 bg-white px-8 py-8 rounded-lg'>
							<h2 className='text-md mt-2 font-semibold pr-48'>Du har blivit utloggad!</h2>

							<div className='flex flex-row'>
								<button
									onClick={handleModalClose}
									className='rounded-full bg-[#026E78] py-[10px] px-[16px] shadow-[0_1px_2px_rgba(198, 228, 246, 0.05)] border border-[#026E78] text-white hover:bg-[#015364] hover:shadow-[0_1px_2px_rgba(198,228,246,0.05] hover:border hover:border-[#015364] focus:shadow-[0_1px_2px_rgba(16,24,40,0.05),0_0px_0px_4px_rgba(rgba(148, 243, 218, 1)] focus:ring-4 focus:ring-[#94f3da] w-32 text-sm mt-2 font-normal not-italic leading-[100%]  text-[14px]'>
									Stäng
								</button>
							</div>
						</div>
					</Modal>
				) : null}
			</div>
			<div className='ml-5'>
				<Link href='/account' className='underline underline-offset-4 font-semibold text-sm'>
					Tillbaka till Admin-sidan
				</Link>
			</div>
		</>
	)
}

export default AdminLogin
