import Image from 'next/image'
import member from '../assets/member.png'
import animal1 from '../assets/animal1.jpg'
import Layout from '@/components/layout'
import Buttons from '@/components/buttons'
import Card from './card'
import Link from 'next/link'

export default function Member() {
	return (
		<Layout>
			<div className=''>
				<Image className=' object-cover w-[393px] h-[556px] md:w-[1414px]' src={member} />
				{/* <div className='bg-slate-300 flex justify-center items-center h-[400px] w-full'> picture </div> */}
				<main className=''>
					<h1 className='text-[#3B3FA4] font-medium text-3xl text-center py-8 md:py-20'>Lorem ipsum el nation</h1>
					<p className='px-10 md:px-48 pb-10 text-lg md:text-2xl'>
						Corem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate libero et velit interdum, ac aliquet odio mattis. Class aptent
						taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur tempus urna at turpis condimentum lobortis. Ut
						commodo efficitur neque. aliquet odio mattis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
						Curabitur tempus urna at turpis condimentum lobortis. Ut commodo efficitur neque.
					</p>
					<div className='flex justify-center w-full'>
						<Link href='/signup'>
							<Buttons title={'Bli medlem'} defaultButton='orange' fixed='xl' />
						</Link>
					</div>
					<h1 className='text-[#3B3FA4] font-medium text-3xl text-center py-8 md:py-20 '>Lorem ipsum el nation</h1>
					<p className='px-10 md:px-48  pb-10 md:pb-20 text-lg md:text-2xl'>
						Copy conubia nostra, per inceptos himenaeos. Curabitur tempus urna at turpis condimentum lobortis. Ut commodo efficitur neque.
					</p>
					<div className='bg-[#3B3FA4] text-white'>
						<h1 className='font-medium text-3xl text-center py-16'>Lorem ipsumish</h1>
						<div className='flex flex-col md:flex-row md:flex-wrap space-y-12 md:space-y-0 md:gap-x-[200px] md:justify-center pb-16'>
							<Card />
							<Card />
							<Card />
							<Card />
							<Card />
							<Card />
						</div>
					</div>
				</main>
			</div>
		</Layout>
	)
}
