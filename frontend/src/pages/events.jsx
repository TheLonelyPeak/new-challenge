import React from 'react'
import fs from 'fs'
import path from 'path'
import Layout from '@/components/layout'
import matter from 'gray-matter'
import Event from '@/components/event'
import { sortByDate } from '../utils'

function Events({ events }) {
	console.log(events)
	return (
		<Layout>
			<div className='w-full grid grid-cols-3 gap-6 m-14'>
				{events.map((event, index) => (
					<Event event={event} />
				))}
			</div>
		</Layout>
	)
}

export default Events

export async function getStaticProps() {
	//Get files from the events folder
	const files = fs.readdirSync(path.join('./src/events'))

	//Get slug and frontmatter (fields etc.) from the events folder
	console.log(files)
	const events = files.map((filename) => {
		//Create slug
		const slug = filename.replace('.md', '')
		console.log(slug)

		//Get frontmatter
		const markdownWithMeta = fs.readFileSync(path.join('./src/events', filename), 'utf-8')

		const { data: frontmatter } = matter(markdownWithMeta)

		return {
			slug,
			frontmatter,
		}
	})

	console.log(events)

	//Sort events by date
	return {
		props: {
			events: events.sort(sortByDate),
		},
	}
}
