import Layout from '../components/layout'
import Image from 'next/image'
import grå from '../assets/grå.jpg'

export default function ContactPage() {
	return (
		<Layout>
			<div className='md:w-[1414px] w-[393px] '>
				<div className=''>
					<Image className='md:w-[1414px] h-[655px]' src={grå} alt='' height={535} width={393} />
				</div>
				<div className=' md:w-[1414px] w-[325px] mt-[56px]  text-center'>
					<p className='md:mt-[94px] ml-[50px]  text-headerBlue font-bold text-2xl'>LOREM IPSUM EL NATION</p>
				</div>
				<div className=' md:h-[134px] md:mb-[39px]  md:mt-[55px] mt-[40px] md:w-[1414px] w-[248px] h-[417px] top-[798px] left-[81px] leadning-[30.36px] text-[24px] ml-[81px] mr-[64px] mb-[99px]'>
					<p className=' md:font-[24px] md:mr-[153px] font-[460px] md:ml-[164px]'>
						Corem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate libero et velit interdum, ac aliquet odio mattis. Class aptent
						taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur tempus urna at turpis condimentum lobortis. Ut
						commodo efficitur neque.
					</p>
				</div>

				<div className='flex flex-col md:flex-row  md:text-center md:justify-evenly '>
					<div className=' md:text-center md:mt-[39px] mt-[99px] mb-[40px]'>
						<p className='pb-3 text-center text-2xl text-headerBlue font-bold'>Lorem - nation</p>

						<div className=' font-medium text-center'>
							<p className='mb-3  '>12.00 - 24.00</p>
							<p className='mb-3'>12.00 - 24.00</p>
							<p>12.00 - 24.00</p>
						</div>
					</div>

					<div className=' md:text-center md:mt-[39px] mt-[99px] '>
						<p className='pb-3 text-center text-2xl text-headerBlue font-bold'>Lorem - nation</p>

						<div className='font-medium text-center mb-[99px]'>
							<p className='mb-3'>12.00 - 24.00</p>
							<p className='mb-3'>12.00 - 24.00</p>
							<p>12.00 - 24.00</p>
						</div>
					</div>
				</div>

				<div className='md:h-[790px] md:w-[1414px] md:flex flex flex-col bg-projectBlue w-[393px] mt-[57px]'>
					<p className='flex self-center md:self-center mt-[57px] mb-[125px] w-[211px] text-3xl leading-10  text-headerBlue font-bold'>
						Lorem - nation
					</p>

					<div className='md:flex justify-evenly'>
						<div className=''>
							<Image className='flex justify-center rounded-full mb-[34px]' src={grå} alt='' width={230} height={239} />
							<div className=' mb-[181px] text-center md:text-center'>
								<a href='mailto:karen@chasacademy.se'>
									<p>Namn</p>
									<br />
									<p> E-post</p>
								</a>
								<p>Telefon: 84833265325</p>
							</div>
						</div>

						<div className=' '>
							<Image className='flex rounded-full mb-[34px]' src={grå} alt='' width={230} height={239} />
							<div className=' mb-[181px] text-center md:text-center'>
								<a href='mailto:karen@chasacademy.se'>
									<p>Namn</p>
									<br />
									<p> E-post</p>
								</a>
								<p>Telefon: 84833265325</p>
							</div>
						</div>

						<div className=''>
							<Image className='flex rounded-full mb-[34px]' src={grå} alt='' width={230} height={239} />
							<div className='mb-[181px] text-center md:text-center'>
								<a href='mailto:karen@chasacademy.se'>
									<p>Namn</p>
									<br />
									<p> E-post</p>
								</a>
								<p>Telefon: 84833265325</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Layout>
	)
}
