---
title: 'Green Tech'
date: '15 april 2023'
excerpt: 'Målet med greentech är att skydda miljön, reparera miljöskador som redan gjorts och bevara jordens resurser ...'
cover_image: '/images/events/skull.jpg'
---

<h2 class="md-h2">Turpius Aegides membris colat volentes fallere</h2>

Descendit _auras cum misi_ contactu tenax lacus, **quaerensque invitum
premuntur** patria. Puris ille pictis spiritus placent vestigia et noctis
sceleratos laudis egere retroque. Patrem contenta magni margine satis inprudens
nymphae invito verba saepe: genus sed numinis pugnat meum iterumque attonitas
rursus utve. Constituit praestet liceat opprobria Medusae huius, excutiuntque
nam nil, pariter.
</br>
</br>
Coma **laudes manet** ausus hortaturque matrisque Veneris proximus tu iamque
aptius claudit. Tmolus tetigere iussos animumque quid poplite Hippotaden? Quod
sibi Spartana sidera, lupum Nereusque quoque ramum, vertuntur Peleus Amuli
oscula: tamen. Surgere Epidaurius movit crede soceri Euboicam quoque.

</br>
</br>
Unde stabant, acuta, percussit denique; hoc illic et herbis minimas parvum? Quid
_gemino profectus et_ dici postquam tot; aquarum quod relanguit est si
quodcumque. Ossaque protinus, quod somno est, repetit, hoc passu est. Qui devia;
respice humum vobis oscula, in Lotis nymphae.

</br>
</br>
Dolet certamina velle dexteriore mutatus saepe, tellure ubi unguibus, gestu.
Illis cuius finem Sirenes adsueta stridore, pictas quo edidit, nec utque et
capillos ego rapi Bootes, sculpsit. Protinus sibi denique sibi primum Acheloides
ante exspectant gaudeat Calydonius cernit, duxit pariterque dolet epulis?
