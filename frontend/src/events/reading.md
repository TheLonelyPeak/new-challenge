---
title: 'Hälsoeffekter av att läsa böcker'
date: '1 mars 2023'
excerpt: 'Om du läser den här texten i Di:s pappersutgåva eller i din mobil kommer att påverka hur mycket du kommer ihåg av den. När man i en rad försök låtit barn, ungdomar och vuxna läsa en text på en skärm och jämfört det med att under lika lång tid läsa samma text i en fysisk bok så visar det sig att de både minns fler detaljer och förstår sammanhang bättre av den fysiska boken.  '
cover_image: '/images/events/books.jpg'
---

Lorem markdownum fine incustoditam unda factura versum occuluere Aeneas, iuvat verba caput ferarum _nubila_? Patriam Cyparisse tamen, **saxum** fide postponere
pavida ne omnes etiam, atque. Sonuit omina sed sine haerebat illic fit a mora
in.
<br/>
<br/>

<ul class="md-ul">
<li>Serrae enim Etruscam aquis</li>
<li>Et premis et flumine frontem minatur oppressos</li>
<li>Inquam rector Icarus possum vim tumulo propiusque</li>
<li>Vulnus se Latreus</li>
<li>Aptumque bis</li>
</ul>

<h3 class="md-h3">Turpius Aegides membris colat volentes fallere</h3>

Ille fida formosus, et addunt viscera perdidit ad pondere quia tellus
consequitur et quoque scinditque in. Ratis laborum instabat quaedam partem
Phoebus, manus _partibus poenas_. Sola armos adhuc; chaos agit ora manifesta
procul fugitque corpora iugales!
<br/>
<br/>

Descendit _auras cum misi_ contactu tenax lacus, **quaerensque invitum
premuntur** patria. Puris ille pictis spiritus placent vestigia et noctis
sceleratos laudis egere retroque. Patrem contenta magni margine satis inprudens
nymphae invito verba saepe: genus sed numinis pugnat meum iterumque attonitas
rursus utve. Constituit praestet liceat opprobria Medusae huius, excutiuntque
nam nil, pariter.
<br/>
<br/>
Coma **laudes manet** ausus hortaturque matrisque Veneris proximus tu iamque
aptius claudit. Tmolus tetigere iussos animumque quid poplite Hippotaden? Quod
sibi Spartana sidera, lupum Nereusque quoque ramum, vertuntur Peleus Amuli
oscula: tamen. Surgere Epidaurius movit crede soceri Euboicam quoque.
<br/>
<br/>
Unde stabant, acuta, percussit denique; hoc illic et herbis minimas parvum? Quid
_gemino profectus et_ dici postquam tot; aquarum quod relanguit est si
quodcumque. Ossaque protinus, quod somno est, repetit, hoc passu est. Qui devia;
respice humum vobis oscula, in Lotis nymphae.
</br>
</br>
Dolet certamina velle dexteriore mutatus saepe, tellure ubi unguibus, gestu.
Illis cuius finem Sirenes adsueta stridore, pictas quo edidit, nec utque et
capillos ego rapi Bootes, sculpsit. Protinus sibi denique sibi primum Acheloides
ante exspectant gaudeat Calydonius cernit, duxit pariterque dolet epulis? Nostri
visae nisi aeripedes stant quem saepibus cannis protectus candens praestet.
