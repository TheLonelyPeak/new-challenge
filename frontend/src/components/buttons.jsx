const sizes = {
	auto: {
		xs: 'w-auto h-[32px] py-[8px] px-[14px] flex-none order-none grow-0 self-stretch font-normal not-italic leading-[100%] text-[14px] ',
		sm: 'w-auto h-[40px] py-[10px] px-[16px] flex-none order-none grow-0 font-normal not-italic leading-[100%]  text-[14px]',
		md: 'w-auto h-[44px] py-[10px] px-[18px] flex-none order-none grow-0 self-stretch not-italic leading-[100%]  font-normal text-[16px]',
		lg: 'w-auto h-[48px] py-[12px] px-[20px] flex-none order-none grow-0 font-normal not-italic leading-[100%]  text-[16px]',
		xl: 'w-auto h-[56px] py-[16px] px-[28px] flex-none order-none grow-0 self-stretch font-normal not-italic leading-[100%] text-[18px]',
	},
	fixed: {
		xs: 'w-[343px] h-[32px] py-[9px] px-[14px] flex-none order-none grow self-stretch font-normal not-italic leading-[100%] text-[14px]',
		sm: 'w-[343px] h-[40px] py-[10px] px-[16px] flex-none order-none grow font-normal not-italic leading-[100%] text-[14px] ',
		md: 'w-[343px] h-[44px] py-[10px] px-[18px] flex-none order-none grow font-normal not-italic leading-[100%] text-[16px]',
		lg: 'w-[343px] h-[48px] py-[12px] px-[20px] flex-none order-none grow font-normal not-italic leading-[100%] text-[16px]',
		xl: 'w-[343px] h-[56px] py-[16px] px-[28px] flex-none order-none grow font-normal not-italic leading-[100%] text-[18px]',
	},
}
const colors = {
	defaultButton: {
		primary:
			'bg-[#026E78] shadow-[0_1px_2px_rgba(198, 228, 246, 0.05)] border border-[#026E78] text-white hover:bg-[#015364] hover:shadow-[0_1px_2px_rgba(198,228,246,0.05] hover:border hover:border-[#015364] focus:shadow-[0_1px_2px_rgba(16,24,40,0.05),0_0px_0px_4px_rgba(rgba(148, 243, 218, 1)] focus:ring-4 focus:ring-[#94f3da]',
		secondaryOutline:
			'bg-white border-2 border-[#026E78] text-[#026E78] hover:border-2 hover:border-[#003C51] hover:drop-shadow-[0_1px_2px_rgba(198, 228, 246, 0.05)] hover:text- focus:ring-4 focus:ring-[#C8F9E8] focus:shadow-[0_1px_2px_rgba(16,24,40,0.05),0_0px_0px_4px_rgba(148, 243, 218, 1)] focus:text-[#026E78]',
		secondaryGray:
			' bg-white border border-[#D0D5DD] shadow-[0_1px_2px_rgba(198, 228, 246, 0.05)] text-[#344054] hover:bg-[#F9FAFB] hover:border hover:border-[#D0D5DD] hover:shadow-[0_1px_2px_rgba(198, 228,246, 0.05)] hover:text-[#1D2939] focus:ring-4 focus:ring-[#94f3da] focus:shadow-[0_0px_0px_4px_rgba(148,243,218,1), 0_1px_2px_rgba(16,24,40,0.05)] focus:text-[#344054]',
		orange:
			' bg-[#E7A422] border-[#E7A422] shadow-[0_1pxbg-[#E7A422] border_2px_rgba(198,228,246,0.05)] text-white hover:bg-gradient-to-br hover:from-[rgba(207,147,30)]  hover:to-[rgba(0,0,0,0.1)] hover:shadow-[0_1px_2px_rgba(198,228,246,0.05)] hover:border hover:border-[rgba(186,132,27)] focus:shadow-[0_1px_2px_rgba(16,24,40,0.05),0_0px_0px_4px_rgba(rgba(148, 243, 218, 1)] focus:ring-4 focus:ring-[#94f3da]',
	},
	disabled: {
		primary: 'bg-[#D0D5DD] shadow-[0px_1px_2px_rgba(198, 228, 246, 0.05)] border border-[#D0D5DD] text-white',
		secondaryOutline: 'border-2 border-[#D0D5DD] text-[#D0D5DD]',
		secondaryGray: 'bg-white border border-[#D0D5DD] shadow-[0px_1px_2px_rgba(189,228,246,0.05)] text-[#D0D5DD]',
	},
}

export default function Buttons(props) {
	const { auto, fixed, defaultButton, disabled, title } = props
	const autoClass = sizes.auto[auto]
	const fixedClass = sizes.fixed[fixed]
	const defaultClass = colors.defaultButton[defaultButton]
	const disabledClass = colors.disabled[disabled]

	return (
		<div className=''>
			<button
				className={` box-border flex justify-center items-center gap-2 rounded-[999px]  ${autoClass} ${fixedClass} ${defaultClass} ${disabledClass}  `}>
				{title}
			</button>
		</div>
	)
}
