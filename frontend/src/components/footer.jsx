import Image from 'next/image'
import Logo from '../assets/Logo.png'
import location from '../assets/location.png'
import chasbanner from '../assets/chasbanner.png'
import linkedin from '../assets/Linkedin.png'
import instagram from '../assets/Instagram.png'
import insta1 from '../assets/insta1.png'
import insta2 from '../assets/insta2.png'
import insta3 from '../assets/insta3.png'
import insta4 from '../assets/insta4.png'
import insta5 from '../assets/insta5.png'
import insta6 from '../assets/insta6.png'
import LightDark from './lightDark'
import AdminLogin from '@/pages/adminLogin'

const Footer = () => {
	return (
		<footer className='flex flex-col md:flex-row md:justify-between p-6 md:p-10 bg-black text-[#FFFFFF] '>
			<div className='md:w-1/5'>
				<div className='flex flex-col items-center md:items-start'>
					<div className='flex items-center justify-center font-bold pb-6 md:pb-16'>
						<Image className='mr-1' src={Logo} width={150} />
					</div>
					<div className='pl-10'>
						<p>Arenavägen 61</p>
						<p>121 77 Johanneshov</p>
						<p className='font-bold pt-4'>Öppettider Studentkåren</p>
						<p className='pt-2'>Måndag: 11.00 - 14.00</p>
						<p>Tisdag: 11.00 - 14.00</p>
						<p>Onsdag: 11.00 - 14.00</p>
					</div>
					<div className='md:pl-10 pt-10'>
						<LightDark />
					</div>
					<div>
						<AdminLogin />
					</div>
				</div>
			</div>
			<div className='md:w-2/5'>
				<div className='flex flex-col items-center'>
					<h3 className='text-center font-bold pt-6 md:pt-0 pb-4'>Hitta hit</h3>
					<Image src={location} className='w-[200px] md:w-[250px] lg:w-[350px]' />
				</div>
			</div>
			<div className='justify-items-center md:w-2/5'>
				<div className='flex flex-col items-center'>
					<h3 className='text-center font-bold pt-6 md:pt-0 pb-4'> Sociala medier</h3>
					<div className='items-center rounded-lg relative'>
						<Image src={chasbanner} className='w-[200px] md:w-[250px] lg:w-[350px]' />
						<Image src={linkedin} className='absolute top-2 left-2' />
					</div>
					<a
						href='https://se.linkedin.com/school/chas-academy/?trk=nav_type_overview'
						target='_blank'
						className='bg-[#026E78] py-2 px-5 mt-4 rounded-full text-white'>
						Skapa kontakt
					</a>
					<div className='w-[200px] md:w-[250px] lg:w-[350px]'>
						<div className='flex items-center pt-8 md:pt-4'>
							<Image src={instagram} className='mr-2' />
							Chask
						</div>
						<div className='flex flex-wrap gap-2 pt-4'>
							<Image src={insta1} className='w-[40px] md:w-[76px] lg:w-[110px]' />
							<Image src={insta2} className='w-[40px] md:w-[76px] lg:w-[110px]' />
							<Image src={insta3} className='w-[40px] md:w-[76px] lg:w-[110px]' />
							<Image src={insta4} className='w-[40px] md:w-[76px] lg:w-[110px]' />
							<Image src={insta5} className='w-[40px] md:w-[76px] lg:w-[110px]' />
							<Image src={insta6} className='w-[40px] md:w-[76px] lg:w-[110px]' />
						</div>
					</div>
					<a href='https://www.instagram.com/chasacademystudentkar/' target='_blank' className='bg-[#026E78] py-2 px-5 mt-4 rounded-full text-white'>
						Följ oss
					</a>
				</div>
			</div>
		</footer>
	)
}

export default Footer
