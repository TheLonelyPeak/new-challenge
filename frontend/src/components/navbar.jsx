import { useState } from 'react'
import hamMeny from '../assets/hamMeny.png'
import chaslogo from '../assets/Logo.png'
import Link from 'next/link'
import Image from 'next/image'
import xx from '../assets/XX.png'
export default function Navbar() {
	const [open, setOpen] = useState(false)
	return (
		<div className='md:w-full md:flex w-[393px] h-[128px] md:h-[160px] '>
			<div className='mt-[42px] font-semibold leading-6 text-[20px] md:text-[16px] cursor-pointer md:mt-[30px] md:mb-[40px] '>
				<div className='flex justify-between md:ml-[64px] '>
					<Link className='flex' href={'/'}>
						<Image width={100} height={50} className='ml-10' src={chaslogo} alt='logo' />
					</Link>
					<div className='md:hidden  '>
						{open ? (
							<Image
								onClick={() => setOpen(false)}
								width={30}
								height={30}
								className=' h-[50px] w-[50px] top-[17,5px] left-[10px] flex mt-5 mr-7 '
								src={xx}
								alt='close'
							/>
						) : (
							<Image
								onClick={() => setOpen(true)}
								className='relative w-[40px] h-[25px] top-[17,5px] left-[10px] mt-8 mr-10 '
								src={hamMeny}
								alt='meny'
							/>
						)}
					</div>
				</div>
			</div>
			{/* DESKTOP MENY */}
			<div className='md:flex justify-between md:grow md:mr-[76px]'>
				<ul className='hidden md:flex md:w-full md:justify-end md:items-center md:gap-[45px] font-inter'>
					<Link href={'/'}>
						<li className='cursor-pointer '> Home</li>
					</Link>
					<Link href={'/events'}>
						<li className='cursor-pointer'>Events </li>
					</Link>
					<Link href={'/member'}>
						<li className='cursor-pointer'>Bli medlem</li>
					</Link>
					<li className='cursor-pointer'>Shop</li>
					<Link href='/contactPage'>
						<li className='cursor-pointer'>Kontakt</li>
					</Link>
				</ul>
				{/* MOBILE MENY */}
				{open ? (
					<div className='absolute bg-white  w-[393px]'>
						<ul className={`flex flex-col md:hidden mt-[150px] gap-[46px] font-inter ml-[54px] w-[313px] h-[622px] `}>
							<li className='cursor-pointer'>
								<Link href='/'>Home</Link>
							</li>
							<div className='flex border border-black' />
							<li className='cursor-pointer'>Events </li>
							<div className='border border-black' />
							<li className='cursor-pointer'>
								<Link href='/signup'>Bli Medlem</Link>
							</li>
							<div className='border border-black' />
							<li className='cursor-pointer'>Shop</li>
							<div className='border border-black' />
							<Link href={'/contactPage'}>
								<li className='cursor-pointer'>Kontakt</li>
							</Link>
							<div className='border border-black'></div>
						</ul>
					</div>
				) : null}
			</div>
		</div>
	)
}
