This is a school project for the student unions website. 

To access the website go to https://chas-karen.grinton.dev/

If the webiste is down, we have created a playbook to automatically set up the website using Caddy. You have to add in your own SSH key in the file inventory.yml before doing the command: ansible-playbook -i inventory.yml caddy_config.yml -v

There is also pipelines in place that run through the code 3 times per day, to ensure the devs code is working properly.

